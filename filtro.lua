function Pow( a )
	return math.pow( a, 2 )
end


local ganhos = {}

local freqs = {
 { 100, 0.16  }
,{ 200, 0.32  }
,{ 300, 0.56  }
,{ 400, 0.96  }
,{ 500, 1.44  }
,{ 600, 1.92  }
,{ 700, 2.48  }
,{ 800, 2.96  }
,{ 900, 3.44  }
,{ 1000, 3.84 }
,{ 1100, 4.16 }
,{ 1200, 4.48 }
,{ 1300, 4.64 }
,{ 1400, 4.8  }
,{ 1500, 4.96 }
,{ 1600, 5.12 }
,{ 1700, 5.2  }
,{ 1800, 5.36 }
,{ 1900, 5.44 }
,{ 2000, 5.52 }

}

function Gain( vi )
	for _,i in ipairs( freqs ) do
		table.insert( ganhos, { i[1], i[2], i[2]/vi, 20*math.log10( i[2]/vi ) } )
	end

	print( "Freq	v0	v0/vi		K" )
	for _,i in ipairs( ganhos ) do
		print( unpack( i ) )
	end

	ganhos = {}
end

Gain( 3.0 )

local fc = 1000
local c1 = 10 / fc / 1000000
local wc = 2 * math.pi * fc
local a = 1.414214
local b = 1
local K = 2
local c2 = c1 / math.abs( K )

local r1 = a / ( 2 * c1 + c2 ) / wc
local r2 = ( ( 2*c1+c2 )*b )/(a*c1*c2*wc)

print( "" )
print( "c1 [uF]", "c2 [uF]", "r1 [kOhm]", "	r2 [kOhm]" )
print( c1, c2, r1/1000, r2/1000 )


local v0 = 1023123 -- medir uma decada acima ou um valor muito alto
print( 'Ganho teorico maximo: ' .. 20*math.log10( c1/c2 ) )
print( string.format( 'Ganho pratico maximo: ' .. 20*math.log10( v0/3.0 ) ) )


print( "" )
print( "Passa Faixa" )


local f0 = 1000
local q0 = 5
local n = 2
K = 2
local w0 = 2 * math.pi * f0 / 1000000

local c = 10 / f0
print( "c: " .. c )

r1 = q0 / ( w0 * c * K )
print( 'r1: ' .. r1/1000 )

r2 = q0 / ( w0 * c * ( 2 * Pow( q0 ) - K ) )
print( 'r2: ' .. r2/1000 )

local r3 = ( 2 * q0 ) /  ( w0 * c )
print( 'r3: ' .. r3/1000 )

if K == r3/( 2*r1 ) then print( 'k igual' ) end

freqs = {
 { 100, 0.12  }
,{ 200, 0.16  }
,{ 300, 0.20  }
,{ 400, 0.24  }
,{ 500, 0.32  }
,{ 600, 0.44  }
,{ 700, 0.64  }
,{ 800, 0.96  }
,{ 900, 1.56  }
,{ 1000, 1.88 }
,{ 1100, 1.28 }
,{ 1200, 0.88 }
,{ 1300, 0.68 }
,{ 1400, 0.56  }
,{ 1500, 0.48 }
,{ 1600, 0.44 }
,{ 1700, 0.4   }
,{ 1800, 0.36 }
,{ 1900, 0.32 }
,{ 2000, 0.32 }

}

Gain( 1.04 )

print( '' )
print( 'Rejeita Faixa' )

f0 = 1000
q0 = 5
w0 = 2 * math.pi * f0 / 1000000
c = 10 / f0

r1 = 1 / ( 2 * q0 * w0 * c )
r2 = ( 2 * q0 ) / ( w0 * c )
r3 = ( r1 * r2 ) / ( r1 + r2 )

print( "r1: " .. r1 /1000 .. " r2: " .. r2/1000 .. " r3: " .. r3/1000 )

freqs = {
 { 100, 1.08  }
,{ 200, 1.08  }
,{ 300, 1.08  }
,{ 400, 1.08  }
,{ 500, 1.08  }
,{ 600, 1.08  }
,{ 700, 1.04  }
,{ 800, 0.92  }
,{ 900, 0.52  }
,{ 1000, 0.48 }
,{ 1100, 0.84 }
,{ 1200, 0.96 }
,{ 1300, 1.0  }
,{ 1400, 1.04 }
,{ 1500, 1.08 }
,{ 1600, 1.08 }
,{ 1700, 1.08 }
,{ 1800, 1.08 }
,{ 1900, 1.08 }
,{ 2000, 1.08 }

}

Gain( 1.08 )

print( '' )
print( 'Passa Tudo' )

local fio = -90
f0 = 1000

a = ( -1 - math.sqrt( ( 1 + 4 * Pow( math.tan( fio/2 ) ) ) ) ) / ( 2 * math.tan( fio/2 ) )

r1 = 1 / ( 2 * a * w0 * c ) 
r2 = 4 * r1
r3 = 8 * r1
local r4 = r3

K = r4 / ( r3 + r4 )
print( "r1: " .. r1/1000 .. " r2: " .. r2/1000 .. " r3: " .. r3/1000 .. " r4: " .. r4/1000 )

freqs = {
 { 100, 1.32  }
,{ 200, 1.32  }
,{ 300, 1.32  }
,{ 400, 1.32  }
,{ 500, 1.32  }
,{ 600, 1.32  }
,{ 700, 1.32  }
,{ 800, 1.32  }
,{ 900, 1.32  }
,{ 1000, 1.32 }
,{ 1100, 1.32 }
,{ 1200, 1.32 }
,{ 1300, 1.32 }
,{ 1400, 1.32 }
,{ 1500, 1.32 }
,{ 1600, 1.32 }
,{ 1700, 1.32 }
,{ 1800, 1.32 }
,{ 1900, 1.32 }
,{ 2000, 1.32 }

}

Gain( 2.6 )
