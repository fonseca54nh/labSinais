local ww = ""

function WTable( b )
	ww = ww .. b .. '\n'
end

function Write( b )
	ww = ww .. b .. '\n\n'
end

local a = { 5 * 1, 5 * 1/2, 5 * 1/4, 5 * 1/8 }

local bits = {
	 { 0,0,0,0 }
	,{ 0,0,0,1 }
	,{ 0,0,1,0 }
	,{ 0,0,1,1 }
	,{ 0,1,0,0 }
	,{ 0,1,0,1 }
	,{ 0,1,1,0 }
	,{ 0,1,1,1 }
	,{ 1,0,0,0 }
	,{ 1,0,0,1 }
	,{ 1,0,1,0 }
	,{ 1,0,1,1 }
	,{ 1,1,0,0 }
	,{ 1,1,0,1 }
	,{ 1,1,1,0 }
	,{ 1,1,1,1 }
}

local t = {}

for _,i in ipairs( bits ) do
	local value = 0
	for index, j in ipairs( i ) do
		if j == 1 then
			value = value + a[index]
		end
	end
	table.insert( t, { i, value }  )
end

t[1][3]  = 0.0008
t[2][3]  = 0.629
t[3][3]  = 1.258
t[4][3]  = 1.886
t[5][3]  = 2.47
t[6][3]  = 3.09
t[7][3]  = 3.67
t[8][3]  = 4.25
t[9][3]  = 4.94
t[10][3] = 5.55
t[11][3] = 6.18
t[12][3] = 6.78
t[13][3] = 7.41
t[14][3] = 8.01
t[15][3] = 8.62
t[16][3] = 9.25


local file = io.open( 'lab06.md', 'wb' )

Write( '---\ntitle: Relatório Laboratório 6\nauthor:\n- Bruno Gabriel da Fonseca\n- Guilherme Ramos TN\n- Jeferson Borges Cardoso\n---' )

Write( '# 3. Preencha a tabela' )

WTable( "|Entrada Digital| Saida Ideal|Saida Medida|" )
WTable( "|---|---|---|" )
for _,i in ipairs( t ) do
	local jj = ""
	if type( i ) == 'table' then
		for _,j in ipairs( i[1] ) do
			jj = jj .. j
		end
	end
	local w = "|" .. jj .. '|' .. i[2] .. '|' .. i[3] .. '|' 
	WTable( w )
end

Write( '\n' )

Write( '# 4. Determine a resolução e o fundo-de-escala. Compare com o valor teórico.' )

Write( "Fundo-de-escala teórico: " .. t[16][2] )
Write( "Resolução teórico: " .. t[2][2] )

Write( "Fundo-de-escala prático: " .. t[16][3] )
Write( "Resolução prática: " .. t[2][3] )

Write( '# 5. O que acontece se o resistor Rf for substítuido por um outro valor?' )

Write( "Ele aumenta ou diminui o ganho proporcionalmente em relacao ao R." )

Write( '# 6 Substitua Rf por um resistor de Rf > 2R. Qual é a nova resolução e fundo-de-escala?' )

Write( "O novo $R_{f} = 10k\\Omega$" )
Write( 'Fundo-de-escala: ' .. 13.15 )
Write( 'Resolução: ' .. 13.15 / 4 )

Write( '# 7. Repita o item anterior para Rf < R/2.' )

Write( "$R_{f} = 470\\Omega$" )
Write( 'Fundo-de-escala: ' .. 3.68 )
Write( 'Resolução: ' .. 3.68/ 16 )

Write( '# Escada R/2R' )

a = { 5 * 1/2, 5 * 1/4, 5 * 1/8, 5 * 1/16 }

t = {}

for _,i in ipairs( bits ) do
	local value = 0
	for index, j in ipairs( i ) do
		if j == 1 then
			value = value + a[index]
		end
	end
	table.insert( t, { i, value }  )
end

t[1][3]  = 0.003
t[2][3]  = 0.3
t[3][3]  = 0.6
t[4][3]  = 0.95
t[5][3]  = 1.26
t[6][3]  = 1.58
t[7][3]  = 1.9
t[8][3]  = 2.2
t[9][3]  = 2.52
t[10][3] = 2.83
t[11][3] = 3.14
t[12][3] = 3.45
t[13][3] = 3.77
t[14][3] = 4.08
t[15][3] = 4.39
t[16][3] = 4.71

WTable( "|Entrada Digital| Saida Ideal |Saida Medida|" )
WTable( "|---|---|---|" )
for _,i in ipairs( t ) do
	local jj = ""
	if type( i ) == 'table' then
		for _,j in ipairs( i[1] ) do
			jj = jj .. j
		end
	end
	local w = "|" .. jj .. '|' .. -i[2] .. '|' .. -i[3] .. '|'
	WTable( w )
end

Write( '\\pagebreak' )

Write( '# 4. Determine a resolução e o fundo-de-escala. Compare com o valor teórico.' )

Write( 'Fundo de escala teórico: ' .. ( -5 * 15) / 16 )
Write( "Resolução teórica: " .. -5 /16 )
Write( 'Fundo de escala prático: ' .. -4.71 )
Write( "Resolução teórica: " .. -0.3 )

Write( '# 5. Compare os valores obtidos entre o DAC Escada R/2R e o DAC com resistores ponderados.' )

Write( 'Com o modelo R/2R os valores de resolução e fundo-de-escala foram praticamente a metade dos valores obtidos com o DAC.' )

Write( '# 6. Considere agora que o nível lógico “1” é igual a 10 V. Qual é a resolução e o fundo-de-escala neste caso?' )

Write( 'Fundo de escala teórico: ' .. ( -10 * 15) / 16 )
Write( "Resolução teórica: " .. -10 /16 )
Write( 'Fundo de escala prático: ' .. -9.45 )
Write( "Resolução teórica: " .. -0.653 )

print( ww )
if file then file:write( ww ) end

io.close( file )

os.execute( 'pandoc lab06.md -o lab06.pdf' )

