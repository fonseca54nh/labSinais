---
title: Relatório Laboratório 6
author:
- Bruno Gabriel da Fonseca
- Guilherme Ramos TN
- Jeferson Borges Cardoso
---

# 3. Preencha a tabela

|Entrada Digital| Saida Ideal|Saida Medida|
|---|---|---|
|0000|0|0.0008|
|0001|0.625|0.629|
|0010|1.25|1.258|
|0011|1.875|1.886|
|0100|2.5|2.47|
|0101|3.125|3.09|
|0110|3.75|3.67|
|0111|4.375|4.25|
|1000|5|4.94|
|1001|5.625|5.55|
|1010|6.25|6.18|
|1011|6.875|6.78|
|1100|7.5|7.41|
|1101|8.125|8.01|
|1110|8.75|8.62|
|1111|9.375|9.25|



# 4. Determine a resolução e o fundo-de-escala. Compare com o valor teórico.

Fundo-de-escala teórico: 9.375

Resolução teórico: 0.625

Fundo-de-escala prático: 9.25

Resolução prática: 0.629

# 5. O que acontece se o resistor Rf for substítuido por um outro valor?

Ele aumenta ou diminui o ganho proporcionalmente em relacao ao R.

# 6 Substitua Rf por um resistor de Rf > 2R. Qual é a nova resolução e fundo-de-escala?

O novo $R_{f} = 10k\Omega$

Fundo-de-escala: 13.15

Resolução: 3.2875

# 7. Repita o item anterior para Rf < R/2.

$R_{f} = 470\Omega$

Fundo-de-escala: 3.68

Resolução: 0.23

# Escada R/2R

|Entrada Digital| Saida Ideal |Saida Medida|
|---|---|---|
|0000|-0|-0.003|
|0001|-0.3125|-0.3|
|0010|-0.625|-0.6|
|0011|-0.9375|-0.95|
|0100|-1.25|-1.26|
|0101|-1.5625|-1.58|
|0110|-1.875|-1.9|
|0111|-2.1875|-2.2|
|1000|-2.5|-2.52|
|1001|-2.8125|-2.83|
|1010|-3.125|-3.14|
|1011|-3.4375|-3.45|
|1100|-3.75|-3.77|
|1101|-4.0625|-4.08|
|1110|-4.375|-4.39|
|1111|-4.6875|-4.71|
\pagebreak

# 4. Determine a resolução e o fundo-de-escala. Compare com o valor teórico.

Fundo de escala teórico: -4.6875

Resolução teórica: -0.3125

Fundo de escala prático: -4.71

Resolução teórica: -0.3

# 5. Compare os valores obtidos entre o DAC Escada R/2R e o DAC com resistores ponderados.

Com o modelo R/2R os valores de resolução e fundo-de-escala foram praticamente a metade dos valores obtidos com o DAC.

# 6. Considere agora que o nível lógico “1” é igual a 10 V. Qual é a resolução e o fundo-de-escala neste caso?

Fundo de escala teórico: -9.375

Resolução teórica: -0.625

Fundo de escala prático: -9.45

Resolução teórica: -0.653

