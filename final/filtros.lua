local pow = math.pow
local sqrt = math.sqrt
local pi = math.pi

Filter = {}
Filter.__index = Filter
Filter.type    = nil
Filter.K       = nil
Filter.w0      = nil
Filter.c1      = nil
Filter.c2      = nil
Filter.r1      = nil
Filter.r2      = nil
Filter.r3      = nil
Filter.fc      = nil
Filter.n       = 2
Filter.a       = 1.4142
Filter.b       = 1
Filter.wc      = nil
Filter.q = nil

local printOrder = { 'type', 'K', 'w0', 'c1', 'c2', 'r1', 'r2', 'r3', 'fc', 'fc2', 'n', 'a', 'b', 'wc', '1' }

function Filter:LowPass( K, fc )
	local t = {}
	setmetatable( t, Filter )

	t.type = 'Low Pass'
	t.K = K
	t.fc = fc
	t.wc = 2 * pi * fc
	t.c2 = ( 10 / fc ) / 1000000
	t.c1 = ( pow(t.a, 2 ) * t.c2 ) / ( 4 * t.b * ( K + 1 ) ) -- c1 <= always round low

	t.r2 = ( 2*( K + 1 ) ) / ( ( t.a*t.c2 + sqrt( pow( t.a, 2 ) * pow( t.c2, 2 ) - 4*t.b*t.c1*t.c2*( K + 1 )) ) * t.wc )
	t.r1 = t.r2 / K
	t.r3 = 1 / ( t.b * t.c1 * t.c2 * pow( t.wc, 2 ) * t.r2 )

	t.r1 = t.r1 / 1000
	t.r2 = t.r2 / 1000
	t.r3 = t.r3 / 1000

	return t
end

function Filter:HighPass( K, fc )
	local t = {}
	setmetatable( t, Filter )

	t.type = 'High Pass'
	t.K = K
	t.fc = fc
	t.wc = 2 * pi * fc
	t.c1 = ( 10 / fc ) / 1000000
	t.c2 = t.c1 / math.abs( K )

	t.r1 = t.a / ( ( 2 * t.c1 + t.c2 ) * t.wc )
	t.r2 = ( ( 2*t.c1 + t.c2 ) * t.b ) / ( t.a * t.c1 * t.c2 * t.wc )

	t.r1 = t.r1 / 1000
	t.r2 = t.r2 / 1000

	return t
end

function Filter:BandPass( K, fc1, fc2 )
	local t = {}
	setmetatable( t, Filter )

	t.type = 'Band Pass'
	t.K = K
	t.fc1 = fc1
	t.fc2 = fc2
	t.fc = sqrt( fc1 * fc2 )

	t.q = t.fc / ( t.fc2 - t.fc1 )
	t.wc = 2 * pi * t.fc / 1000000

	t.c = 10 / t.fc

	t.r1 = t.q / ( t.wc * t.c * K )
	t.r2 = t.q / ( t.wc * t.c * ( 2 * pow( t.q, 2 ) - t.K ) )
	t.r3 = ( 2 * t.q ) / ( t.wc * t.c )

	t.r1 = t.r1 / 1000
	t.r2 = t.r2 / 1000
	t.r3 = t.r3 / 1000

	return t
end


function Filter:Show()
	local temp = {}
	for index,i in pairs( self ) do

		table.insert( temp, { index, i } )
	end

	table.sort( temp, function( a, b ) return a[1] < b[1] end)

	print( 'type	' .. self.type )
	for _,i in pairs( temp ) do
		if i[1] ~= 'type' then print( i[1], i[2] ) end
	end

	print()

	temp = nil
end

local pb = Filter:LowPass( 2, 1000 ):Show()
local pa = Filter:HighPass( 2, 1000 ):Show()
local bp = Filter:BandPass( 2, 750, 1250 ):Show()


